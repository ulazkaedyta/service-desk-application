package pl.sda.finalproject.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;

@Entity
@Data
@Table(name = "companies")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @Max (100)
    private String companyName;
}
