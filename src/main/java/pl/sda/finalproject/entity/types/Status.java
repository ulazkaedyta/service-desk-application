package pl.sda.finalproject.entity.types;

public enum Status {
    DONE,
    INPROGRESS,
    TOEXPLAINWITHCUSTOMER,
    TODO
}
