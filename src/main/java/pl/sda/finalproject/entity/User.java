package pl.sda.finalproject.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Set;

@Data
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
//    @NotBlank
//    @Size(max = 20, min = 3,
//            message = "Username should be longer than 3 characters and" +
//                    "shorter than 20 characters")
    private String username;

    @Column(nullable = false)
//    @Size(min = 3,
//            message = "Password should be longer than 3 characters and" +
//                    "shorter than 10 characters")
    private String password;

    @Column
//    @Email
//    @Size(max = 12, min = 5,
//            message = "Email should be longer than 5 characters and" +
//                    "shorter than 12 characters")
    private String email;

    @Column
//    @Size(max = 10, min = 2,
//            message = "First name should be longer than 2 characters and" +
//                    "shorter than 10 characters")
    private String firstName;

    @Column
//    @Size(max = 10, min = 2,
//            message = "Last name should be longer than 2 characters and" +
//                    "shorter than 10 characters")
    private String lastName;

    @Column
    //@NotNull
    private boolean isAdmin;

    @Column
    private String position;

    @OneToMany
    @JoinColumn(name = "user_id")
    private Set<Task> tasksToDo;

}
