package pl.sda.finalproject.entity;

import lombok.Data;
import pl.sda.finalproject.entity.types.Status;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    @Size(max=400, min=3, message = "Description should be longer than 3 characters and shorter than 400 characters")
    private String description;

    @Column(nullable = false)
    @Size(max=5, message = "Priority should be in range from 0 to 1. For no-action tasks 0 is used")
    private int priority;

    @Column
    @NotBlank
    @Enumerated (EnumType.STRING)
    private Status status;

    @Column
    private LocalDateTime localDateTime;

    @OneToOne //(cascade = CascadeType.ALL)
    @JoinColumn(name= "commentary_id")
    private Commentary commentary;
}
