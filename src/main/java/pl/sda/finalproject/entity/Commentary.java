package pl.sda.finalproject.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "commentaries")
@Data
public class Commentary {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    @Size (max=400, min=3, message = "Commentary should be longer than 3 characters and shorter than 400 characters")
    @NotEmpty
    private String text;

    @Column
    private LocalDateTime localDateTime;

    @Column
    @FutureOrPresent
    private LocalDate declaredTimeOfFixing;

}
