package pl.sda.finalproject.mapper;

import org.mapstruct.Mapper;
import pl.sda.finalproject.dto.TaskDto;
import pl.sda.finalproject.entity.Task;

@Mapper(componentModel="spring")
public abstract class TaskMapper {
    public abstract Task toEntity(TaskDto dto);
    public abstract TaskDto toDto(Task entity);

}
