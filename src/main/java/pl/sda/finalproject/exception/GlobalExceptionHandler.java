package pl.sda.finalproject.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import pl.sda.finalproject.dto.ErrorDto;


@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({Exception.class})
    public ResponseEntity<ErrorDto> handleEntityNotFound(Exception e) {
        ErrorDto dto = new ErrorDto();
        dto.setMessage(e.getMessage());
        return ResponseEntity.badRequest().body(dto);
    }
}
