package pl.sda.finalproject.service;

import org.springframework.stereotype.Service;
import pl.sda.finalproject.dto.TaskDto;
import pl.sda.finalproject.entity.Task;
import pl.sda.finalproject.mapper.TaskMapper;
import pl.sda.finalproject.repository.TaskRepository;


import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaskService {

    private TaskRepository taskRepository;
    private TaskMapper taskMapper;

    public TaskService(TaskRepository taskRepository, TaskMapper taskMapper) {
        this.taskRepository = taskRepository;
        this.taskMapper = taskMapper;
    }

    public List<TaskDto> findAll() {
        List<Task> serviceTasks = taskRepository.findAll();
        return serviceTasks.stream().map(taskMapper::toDto).collect(Collectors.toList());
    }

    public TaskDto findOne(Long id) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity not found"));

        return taskMapper.toDto(task);
    }

    public void create(TaskDto dto) {
        Task task = taskMapper.toEntity(dto);
        taskRepository.save(task);
    }

    public void update(Long id, TaskDto dto) {
        Task task = taskRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity not found"));

        task.setDescription(dto.getDescription());
        taskRepository.save(task);
    }

    public void delete(Long id) {
        taskRepository.deleteById(id);
    }


}
