package pl.sda.finalproject.dto;

import lombok.Data;
import pl.sda.finalproject.entity.types.Status;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
public class TaskDto {

    private Long id;

    @Size(max = 400, min = 3, message = "Description should be longer than 3 characters " +
            "and shorter than 400 characters")
    private String description;

    @NotBlank
    @Enumerated(EnumType.STRING)
    private Status status;

    private LocalDateTime localDateTime;
    @NotBlank
    @Size(max = 5, min = 1, message = "Priority should be in range from 1 to 5 ")
    private int Priority;

}
