package pl.sda.finalproject.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UserDto {

    @NotBlank
    @Size (max=20, min=3,
            message = "Username should be longer than 3 characters and" +
                    "shorter than 20 characters")
    private String username;


    @Size ( min=3,
            message = "Password should be longer than 3 characters and" +
                    "shorter than 10 characters")
    private String password;


    @Email
    @Size (max=12, min=5,
            message = "Email should be longer than 5 characters and" +
                    "shorter than 12 characters")
    private String email;


    @Size (max=10, min=2,
            message = "First name should be longer than 2 characters and" +
                    "shorter than 10 characters")
    private String firstName;


    @Size (max=10, min=2,
            message = "Last name should be longer than 2 characters and" +
                    "shorter than 10 characters")
    private String lastName;

    private String position;

    @NotNull
    private boolean isAdmin;


}
