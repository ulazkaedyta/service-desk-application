package pl.sda.finalproject.dto;

import lombok.Data;

import javax.validation.constraints.Max;

@Data
public class CompanyDto {
    @Max(100)
    private String companyName;
}
