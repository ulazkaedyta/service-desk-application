package pl.sda.finalproject.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
public class CommentaryDto {

    @Size(max=400, min=3, message = "Commentary should be longer than 3 characters and shorter than 400 characters")
    @NotEmpty
    private String text;

    private LocalDateTime localDateTime;

    @FutureOrPresent
    private LocalDate declaredTimeOfFixing;

}
