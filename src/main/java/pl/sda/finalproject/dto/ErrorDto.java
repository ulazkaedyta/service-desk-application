package pl.sda.finalproject.dto;

import lombok.Data;

@Data
public class ErrorDto {

    private String message;
}
