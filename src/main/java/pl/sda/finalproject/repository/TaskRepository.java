package pl.sda.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.finalproject.entity.Task;


public interface TaskRepository extends JpaRepository<Task, Long> {
}
