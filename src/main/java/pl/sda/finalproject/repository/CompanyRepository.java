package pl.sda.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.finalproject.entity.Commentary;
import pl.sda.finalproject.entity.Company;

public interface CompanyRepository extends JpaRepository<Company, Long> {
}
