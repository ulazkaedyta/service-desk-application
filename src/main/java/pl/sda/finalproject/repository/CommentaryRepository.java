package pl.sda.finalproject.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.sda.finalproject.entity.Commentary;

public interface CommentaryRepository extends JpaRepository<Commentary, Long> {

}
