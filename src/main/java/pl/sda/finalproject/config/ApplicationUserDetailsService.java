package pl.sda.finalproject.config;


import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import pl.sda.finalproject.repository.UserRepository;
import org.springframework.security.core.userdetails.User;


public class ApplicationUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    public ApplicationUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        pl.sda.finalproject.entity.User user =
                userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));

        return User.builder()
                .username(user.getUsername())
                .password(user.getPassword())
                .roles("USER") //TODO role również powinny znaleźć się w bazie danych
                .build();
    }
}
