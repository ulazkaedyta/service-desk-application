package pl.sda.finalproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pl.sda.finalproject.dto.TaskDto;
import pl.sda.finalproject.service.TaskService;


import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping({"/tasks", "/"})
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public String list(@RequestParam(required = false) Long deleteId, Model model, Principal principal) {
        //TODO użyć obiektu principal aby zwrócić tylko zadania wybranego użytkownika
        if (deleteId != null) {
            taskService.delete(deleteId);
            return "redirect:tasks";
        }

        List<TaskDto> tasks = taskService.findAll();
        model.addAttribute("tasks", tasks);
        model.addAttribute("task", new TaskDto());
        return "tasks";
    }

    @PostMapping
    public String create(@Valid @ModelAttribute("task") TaskDto task, BindingResult error, Model model, Principal principal) {
        //TODO użyć obiektu principal aby przypisać dodawane zadanie do zalogowanego użytkownika
        if (error.hasErrors()) {
            return "redirect:tasks?error";
        }

        taskService.create(task);
        return "redirect:tasks";
    }
}
