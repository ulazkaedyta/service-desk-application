package pl.sda.finalproject.controller;

import org.springframework.web.bind.annotation.*;
import pl.sda.finalproject.dto.TaskDto;
import pl.sda.finalproject.service.TaskService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestController {

    private TaskService taskService;

    public TaskRestController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping
    public List<TaskDto> getAll() {
        return taskService.findAll();
    }

    @GetMapping("/{id}")
    public TaskDto getOne(@PathVariable Long id) {
        return taskService.findOne(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody TaskDto task) {
        taskService.create(task);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable Long id, @Valid @RequestBody TaskDto task) {
        taskService.update(id, task);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        taskService.delete(id);
    }
}
