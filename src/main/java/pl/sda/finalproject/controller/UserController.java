package pl.sda.finalproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.finalproject.dto.UserDto;
import pl.sda.finalproject.service.UserService;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/register")
    public String register(Model model) {
        model.addAttribute("user", new UserDto());
        return "glownyUzywanyFront/zarejestrujUsera";
    }

    @PostMapping("/register")
    public String register(@Valid @ModelAttribute("user") UserDto user, BindingResult errors) {
        userService.save(user);
        return "redirect:login";
    }



    @GetMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new UserDto());
        return "glownyUzywanyFront/loginPageDlaUseraLubAdmina";
    }

    @PostMapping("/login")
    public String login(@Valid @ModelAttribute("user") UserDto user, BindingResult errors) {
        userService.save(user);
        return "redirect:login";
    }
}
